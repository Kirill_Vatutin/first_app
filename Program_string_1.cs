﻿using System;

namespace task_string_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string hello = "Привет!";
            string name = "Меня зовут ";
            string age = "Мне ...  лет";
            string input_name = Console.ReadLine();
            string input_age = Console.ReadLine();
            Console.WriteLine(hello +" "+ name+input_name+". "+age.Replace("...",input_age));
            Console.WriteLine($"{hello} {name}{input_name}. Мне {input_age} лет");



        }
    }
}
