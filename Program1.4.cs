﻿using System;

namespace task1._3
{
    class Program
    {
        static int checking_number(int a)
        {
            while (!int.TryParse(Console.ReadLine(), out a))
            {
                Console.WriteLine("Введите целое число!");
            }
            return a;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество символов, которое необходимо ввести");
            int n = 0;
            n = checking_number(n);
            int count_odd = 0;
            int sum = 0;
            int first_not_odd=0, second_not_odd=0, last_not_odd = 0;
            Console.WriteLine();

            for (int i = 0, number=0; i < n; i++)
            {
                number = checking_number(number);
                if (number%2==1)
                {
                    switch (count_odd)
                    {
                        case 0:
                            {
                                last_not_odd = number;
                                count_odd++;
                                break;
                            }
                        case 1:
                            {
                                second_not_odd = number;
                                count_odd++;
                                break;
                            }
                        case 2:
                            {
                                first_not_odd = number;
                                count_odd++;
                                break;
                            }
                        default:
                            {
                                int c = first_not_odd;
                                first_not_odd = number;
                                last_not_odd = second_not_odd;
                                second_not_odd = c;
                                break;
                            }

                    }
                    sum = first_not_odd + second_not_odd + last_not_odd;
                    
                }
            }
            Console.WriteLine($"Сумма последних 3 нечетных чисел = {sum}");
        }
    }
}
