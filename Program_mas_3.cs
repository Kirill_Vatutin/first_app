﻿using System;

namespace task2._1
{
    class Program_mas_2
    {
        static int checking_number()
        {
            int a;
            while (!(int.TryParse(Console.ReadLine(), out a )) )
            {
                Console.WriteLine("Введите целое число!");
            }
            return a;
        }
        static int checking_number(int max=100,int min=-100)
        {
            int a;
            while (!(int.TryParse(Console.ReadLine(), out a)) || !(a >= min && a <= max))
            {
                Console.WriteLine("Введите целое число в диапазоне[-100,100] !");
            }
            return a;
        }
        static void Main(string[] args)
        {
            int n=checking_number() ;
            int[] mas = new int[n];
            for (int i = 0; i < n; i++)
            {
                mas[i] = checking_number(100,-100);
            }
            int max=-101 ;//максимальное число
            int max_i=-1;//индекс максимального числа
            int min=101 ;//минимальное число
            int min_i=-1;//индекс минимального числа
            int min_odd=101;//минимальное четное число
            int min_not_odd = 101;//минимальное нечетное число
            for (int i = 0; i < n; i++)
            {
                if (mas[i]>max)
                {
                    max = mas[i];
                    max_i = i;
                }
                if (mas[i]<min)
                {
                    min = mas[i];
                    min_i = i;
                }
                if (mas[i] < min_odd && mas[i] % 2 == 0) min_odd = mas[i];
                if (mas[i] < min_odd && mas[i] % 2 == 1) min_not_odd = mas[i];

            }
            mas[max_i] = min;
            mas[min_i] = max;
           
            Console.WriteLine();
            Console.WriteLine($"Максимальное число - {max}\nМинимальное число - {min}\n" +
                $"Минимальное четное число - {min_odd}\nМинимальное нечетное число - {min_not_odd}");



            
        }
    }
}
