﻿using System;

namespace task1._3
{
    class Program
    {
        static int checking_number(int a)
        {
            while (!int.TryParse(Console.ReadLine(), out a))
            {
                Console.WriteLine("Введите целое число!");
            }
            return a;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество символов, которое необходимо ввести");
            int n=0;
            n=checking_number(n);
            int number=0;
            int count_odd = 0;
            int sum=0;
            for (int i = 0; i <n; i++)
            {
               number= checking_number(number);
              if (count_odd<3 && number%2==1)
                {
                    count_odd++;
                    sum += number;
                }
            }
            Console.WriteLine(sum);
        }
    }
}
