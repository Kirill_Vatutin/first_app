using System;

namespace task1._5
{
    class Program
    {
        static int checking_number()
        {
            int a;
            while ( !(int.TryParse(Console.ReadLine() , out a)) || a > 20)
            {
                Console.WriteLine("Введите целое число не больше 20!");
            }
            return a;
        }

        static void Main(string[] args)
        {
            int m, n ;
            m = checking_number();
            n = checking_number();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            Console.WriteLine($"m={m} теперь катет треугольника");
            Console.WriteLine();
            for (int i = 1; i < m; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
    }
}
