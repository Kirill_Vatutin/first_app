﻿using System;

namespace task7
{
    class Program
    {
        static int checking_number()
        {
            int a;
            while (!(int.TryParse(Console.ReadLine(), out a)))
            {
                Console.WriteLine("Введите целое число !");
            }
            return a;
        }

        static void Main(string[] args)
        {
            int fib1 = 0, fib2 = 1;
            int  n = checking_number();
            int fib_new = 0;
            int sum = 1;
            for (int i = 0; i < n-1; i++)
            {
                fib_new = fib1 + fib2;
                sum += fib_new;
                fib1 = fib2;
                fib2 = fib_new;


                
            }
            Console.WriteLine(sum);


        }
    }
}
