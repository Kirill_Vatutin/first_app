using System;

namespace task2._1
{
    class Program
    {
        static int checking_number()
        {
            int a;
            while (!(int.TryParse(Console.ReadLine(), out a)))
            {
                Console.WriteLine("Введите целое число !");
            }
            return a;
        }
        static void Main(string[] args)
        {
            int n=checking_number() ;
            int max = 0;
            int c_max = 0;
            int[] mas = new int[n];
            for (int i = 0; i < n; i++)
            {
                mas[i] = checking_number();
            }
            for (int i = 0; i < n; i++)
            {
               if (mas[i] == mas[j])
                    {
                        c++;
                    }
                    if (c >= c_max)
                    {
                        max = mas[i];
                        c_max = c;
                        if (mas[i] > max)
                        {
                            max = mas[i];
                            c_max = c;
                        }
                    }
            }
            Console.WriteLine($"Число {max} встретилось в количестве {c_max} раз");
        }
    }
}
