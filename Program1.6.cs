﻿using System;

namespace task1._6
{
  

    class Program
    {
        static long checking_number()
        {
            long a;
            while (!(long.TryParse(Console.ReadLine(), out a)))
            {
                Console.WriteLine("Введите целое число !");
            }
            return a;
        }
        static void Main(string[] args)
        {
            int sum = 0;
            for (int i = 1; i <= 50; i++)
            {
                sum += i;
              
            }
            Console.WriteLine($"Сумма ряда от 1 до 50 = {sum}");
            sum = 0;
            for (int i = 2; i <=50; i+=2)
            {
                sum += i;
            }
            Console.WriteLine($"Сумма ряда от 2 до 50 c шагом 2 = {sum}");
            Console.WriteLine("Введите число, которое ограничит ряд");
            long n = checking_number();
            long big_sum = 0;
                for (int i =1; i <= n; i+=2)
                {
                big_sum += i;
                }
            Console.WriteLine($"Сумма ряда = {big_sum}");
        }
    }
}
