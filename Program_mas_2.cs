﻿using System;

namespace task2._1
{
    class Program_mas_1
    {
        static int checking_number()
        {
            int a;
            while (!(int.TryParse(Console.ReadLine(), out a)))
            {
                Console.WriteLine("Введите целое число !");
            }
            return a;
        }
        static void Main(string[] args)
        {
            int n=checking_number() ;
            int max = 0;
            int c_max = 0;
            int[] mas = new int[n];
            for (int i = 0; i < n; i++)
            {
                mas[i] = checking_number();
            }
            bool desk = true;//проверка по убыванию
            bool asc = true;// проверка по возрастанию
            for (int i = 0; i < n-1; i++)
            {
                if (mas[i] > mas[i + 1])
                {
                    asc = false;

                }
                else
                {
                    desk = false;
                }
            }
            Console.WriteLine($"массив возрастает? {asc}\nмассив убывает? {desk}");
        }
    }
}
