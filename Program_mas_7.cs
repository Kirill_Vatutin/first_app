﻿using System;
using System.IO;

namespace task2._1
{
    class Program_mas_5_6
    {
        static int checking_number()
        {
            int a;
            while (!(int.TryParse(Console.ReadLine(), out a )) )
            {
                Console.WriteLine("Введите целое число!");
            }
            return a;
        }
        static void ShiftElements(int[] mas)
        {
            int prev = mas[0];
            int next;
            for (int i = 0; i < mas.Length - 1; i++)
            {
                next = mas[i + 1];
                mas[i + 1] = prev;
                prev = next;
            }
            mas[0] = prev;
        }
        
      
        static void Main(string[] args)
        {
            Console.Write("Введите количество элементов массива: ");
            int n = checking_number();
            int[] mas = new int[n];
            for (int i = 0; i < n; i++)
            {
                mas[i] = checking_number();
            }
            Console.WriteLine("Введите число, на которое нужно сдвинуть элементы массива");
            int k = (checking_number())%n;//берем остаток чтобы был всего 1 круг сдвига,иначе излишне
            if (k>=0)
                for (int i = 0; i < k; i++)
                {
                    ShiftElements(mas);
                }
            else
            {
                for (int i = 0; i < n+k; i++)
                {
                    ShiftElements(mas);
                }
            }
            


           

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(mas[i]+" ");
            }

            
        }
    }
}
