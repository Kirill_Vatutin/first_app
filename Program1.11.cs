﻿using System;

namespace task7
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            int count = 0;
            int i = 6;

            while (sum+i<=100)
            {
                sum += i;
                i += 4;
                count++;
            }
            Console.WriteLine($" сумма равна = {sum}\n Количество слагаемых равно {count}");

        }
    }
}
