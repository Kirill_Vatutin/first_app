﻿using System;

namespace task_string_1
{
    class Program
    {
       

        static void Main(string[] args)
        {
            string s1 = "Хорошо в лесу";
            char ch = 'о';
            int indexOfChar = s1.IndexOf(ch);
            Console.WriteLine(indexOfChar);
            s1 = "Эх, дороги, пыль да туман";
            indexOfChar = s1.IndexOf(ch);
            Console.WriteLine(indexOfChar);
            s1 = "Семнадцать вариантов решения";
            indexOfChar = s1.IndexOf(ch);
            Console.WriteLine(indexOfChar);
            s1 = "Тут нет буквы o";//o написана на английсском, получается, все правильно работает,возвращает -1
            indexOfChar = s1.IndexOf(ch);
            Console.WriteLine(indexOfChar);

        }
    }
}
