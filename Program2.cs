﻿using System;

namespace first_app
{
    class Program
    {
        static int sum_number(int number)// функция из первой задачи задача
        {
            int sum = 0;
            while (number > 0)
            {
                sum += number % 10;
                number = number / 10;

            }
            return sum;
        }
        static void Main(string[] args)
        {

            int number;
            int max_sum_number = 0;
            do
            {
                while (!int.TryParse(Console.ReadLine(), out number))
                {
                    Console.WriteLine(" Введите целое число!");
                }
                if (sum_number(number) > max_sum_number) max_sum_number = sum_number(number);
     
            }
            while (number != 0);
            Console.WriteLine(max_sum_number);


        }
    }
}
