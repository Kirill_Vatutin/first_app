using System;
using System.IO;

namespace task2._1
{
    class Program_mas_3
    {


        static void Main(string[] args)
        {

            string path = "massiv.txt";
            string[] r = null;

            if (File.Exists(path))
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    r = sr.ReadToEnd().ToString().Split();
                }
                Array.Sort(r);
                // Array.Reverse(r);
                using (StreamWriter sw = new StreamWriter(path, false))
                {
                    foreach (var item in r)
                    {
                        int a = 0;
                        if (int.TryParse(item, out a))
                        {
                            sw.Write($"{item} ");
                        }
                    }
                }
                Console.WriteLine("Программа отработала");
            }
            else
            {
                Console.WriteLine("Упс...Кажется, файла нет");
            }
 
        }
        
    }
}
