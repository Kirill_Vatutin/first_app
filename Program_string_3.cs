﻿using System;

namespace task_string_1
{
    class Program
    {
        static void compare_string(string s1,string s2)
        {
            int result = String.Compare(s1, s2);
            if (result < 0)
            {
                Console.WriteLine("Строка s1 перед строкой s2");
            }
            else if (result > 0)
            {
                Console.WriteLine("Строка s1 стоит после строки s2");
            }
            else
            {
                Console.WriteLine("Строки s1 и s2 идентичны");
            }
        }
        static void Main(string[] args)
        {
            compare_string("привет", "здравствуйте");
            compare_string("двадцать", "двенадцать");
            compare_string("синус", "синусоида");
            compare_string("14", "81");





        }
    }
}
