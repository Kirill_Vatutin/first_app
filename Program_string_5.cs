﻿using System;

namespace task_string_1
{
    class Program
    {
       

        static void Main(string[] args)
        {
            string s1 = "Где такое интересное место?";
            char ch = 'у';
            int indexOfChar = s1.LastIndexOf(ch);
            Console.WriteLine(indexOfChar);
            s1 = "У меня дома есть ноутбук.";
            indexOfChar = s1.LastIndexOf(ch);
            Console.WriteLine(indexOfChar);
            s1 = "Винтажный стул";
            indexOfChar = s1.LastIndexOf(ch);
            Console.WriteLine(indexOfChar);
            

        }
    }
}
