﻿using System;
using System.IO;

namespace task2._1
{
    class Program_mas_7
    {
        static int checking_number()
        {
            int a;
            while (!(int.TryParse(Console.ReadLine(), out a )) )
            {
                Console.WriteLine("Введите целое число!");
            }
            return a;
        }
        static int checking_number(int prev)
        {
            int a;
            while (!(int.TryParse(Console.ReadLine(), out a))  || a <= prev) 
            {
                Console.WriteLine("Введите целое число!");
            }
            return a;
        }


        static void Main(string[] args)
        {
            Console.Write("Введите количество элементов первого массива: ");
            int n = checking_number();
            int[] mas = new int[n];
            int prev=checking_number();
            mas[0] = prev;
            for (int i = 1; i < n; i++)
            {
                mas[i] = checking_number(prev);
                prev = mas[i];
                
            }

            Console.Write("Введите количество элементов второго массива: ");
             n = checking_number();
            int[] mas2 = new int[n];
             prev = checking_number();
            mas2[0] = prev;
            for (int i = 1; i < mas2.Length; i++)
            {
                mas2[i] = checking_number(prev);
                prev = mas2[i];
            }
            int[] mas_res = new int[mas.Length+mas2.Length];
  



            for (int k = 0,i = 0, j = 0; k < mas_res.Length; k++)
            {

                if (i > mas.Length - 1)
                {
                    int a = mas2[j];
                    mas_res[k] = a;
                    j++;
                }
                else if (j > mas2.Length - 1)
                {
                    int a =mas[i];
                    mas_res[k] = a;
                    i++;
                }
                else if (mas[i] < mas2[j])
                {
                    int a = mas[i];
                    mas_res[k] = a;
                    i++;
                }
                else
                {
                    int b = mas2[j];
                    mas_res[k] = b;
                    j++;
                }
            }

            for (int i = 0; i <mas.Length+mas2.Length; i++)
            {
                Console.Write(mas_res[i]+" ");
            }




        }
    }
}
