﻿using System;

namespace first_app
{
    class Program
    {
        static int sum_number(int number)
        {
            int sum = 0;
            while (number > 0)
            {
                sum += number % 10;
                number = number / 10;

            }
            return sum;
        }
        static void Main(string[] args)
        {
            int number = int.Parse((Console.ReadLine()));
            Console.WriteLine(sum_number(number));
        }
    }
}
